# Python Practice - 01
***
This practice consists of 15 programs.
### Programs
- WAPP which accepts a sequence of comma-separated numbers from user and generate a list and a tuple with those numbers.


- WAPP to Check Armstrong Number


- WAPP to Find the Factorial of a Number


- WAPP to Convert Celsius To Fahrenheit


- WAPP to calculate body mass index


- WAPP to check if a string is palindrome or not


- WAPP to remove all duplicates from a given string.


- WAP function for removing i-th character from a string


- WAP Function to find the sum of digits of a number.


- WAP Class for Linear Search


- WAP Class for Binary Search


- WAP Class for Bubble Sort


- WAP Class to generate ACRONYM for the input name.


- WAP Class to find Cumulative sum of a list.


- WAP Class to convert an integer to a roman numeral

### Requirements

- Pycharm
- Python 3.6 or above

### Submission Criteria

- The submissions are to be made before the end of the day [EOD].
- Each program must be in a seperate file.
- You must commit and push your code to BitBucket after each program.
- You must create a requirements.txt file.
